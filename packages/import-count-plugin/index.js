'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var PluginName = 'import-count-plugin';
var ImportCountPlugin = /** @class */ (function () {
    function ImportCountPlugin(options) {
        this.options = options;
        this.components = [];
        console.log("my option");
    }
    ImportCountPlugin.prototype.apply = function (compiler) {
        var _this = this;
        if (compiler.options.watch) { //watch模式不执行
            return;
        }
        compiler.hooks.normalModuleFactory.tap(PluginName, function (factory) {
            factory.hooks.parser["for"]('javascript/auto').tap(PluginName, function (parser) {
                parser.hooks.importSpecifier.tap(PluginName, function (_statement, source, _exportName, identifierName) {
                    if (source === _this.options.lib) {
                        var key = identifierName;
                        _this.components[key] = _this.components[key] ? _this.components[key] + 1 : 1;
                    }
                });
            });
        });
        compiler.hooks.done.tap(PluginName, function () {
            console.log(_this.components);
        });
    };
    return ImportCountPlugin;
}());

exports.ImportCountPlugin = ImportCountPlugin;
