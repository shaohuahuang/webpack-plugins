import { Compiler } from "webpack";

const PluginName = 'import-count-plugin'
export class ImportCountPlugin {
  public components = []
  constructor(private options){
    console.log("my option")
  }

  apply(compiler: Compiler){
    if(compiler.options.watch){ //watch模式不执行
      return
    }

    compiler.hooks.normalModuleFactory.tap(PluginName, (factory) => {
      factory.hooks.parser.for('javascript/auto').tap(PluginName, (parser) => {
        parser.hooks.importSpecifier.tap(PluginName, 
          (_statement: string, source: string, _exportName: string, identifierName: string) => {
            if(source === this.options.lib){
              const key = identifierName
              this.components[key] = this.components[key]? this.components[key] + 1: 1
            }            
          }
        )
      })
    })

    compiler.hooks.done.tap(PluginName, () => {
      console.log(this.components)
    })
  }
}
