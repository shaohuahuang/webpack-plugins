import { Compiler } from "webpack";

const PluginName = 'lines-count-plugin'
export class LinesCountPlugin {
  private result = {}
  constructor(private options){
  }

  apply(compiler: Compiler){
    if(compiler.options.watch){ //watch模式不执行
      return
    }

    compiler.hooks.normalModuleFactory.tap(PluginName, (factory) => {
      factory.hooks.parser.for('javascript/auto').tap(PluginName, (parser) => {

        parser.hooks.program.tap(PluginName, (ast, comments) => {
          let absPath = parser.state.module.request
          if(absPath.includes(this.options.srcFolder)){
              this.result[absPath] = { total: ast.loc.end.line - ast.loc.start.line }
          }
        });
      })
    })

    compiler.hooks.done.tap(PluginName, () => {
      console.log(this.result)
    })
  }
}
