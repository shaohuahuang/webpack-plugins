import typescript from 'rollup-plugin-typescript2';

export default {
    input: ["index.ts"],
    output: [
        {
            dir: "./",
            entryFileNames: "[name].js",
            format: "cjs",
            exports: "named"
        }
    ],
    plugins: [
        typescript(),
    ],
    external: []
};
