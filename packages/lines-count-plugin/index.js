'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var PluginName = 'lines-count-plugin';
var LinesCountPlugin = /** @class */ (function () {
    function LinesCountPlugin(options) {
        this.options = options;
        this.result = {};
    }
    LinesCountPlugin.prototype.apply = function (compiler) {
        var _this = this;
        if (compiler.options.watch) { //watch模式不执行
            return;
        }
        compiler.hooks.normalModuleFactory.tap(PluginName, function (factory) {
            factory.hooks.parser["for"]('javascript/auto').tap(PluginName, function (parser) {
                parser.hooks.program.tap(PluginName, function (ast, comments) {
                    var absPath = parser.state.module.request;
                    if (absPath.includes(_this.options.srcFolder)) {
                        _this.result[absPath] = { total: ast.loc.end.line - ast.loc.start.line };
                    }
                });
            });
        });
        compiler.hooks.done.tap(PluginName, function () {
            console.log(_this.result);
        });
    };
    return LinesCountPlugin;
}());

exports.LinesCountPlugin = LinesCountPlugin;
